﻿(function () {
    var app = angular.module('OfflineApp', [
        /*'ngAnimate',*/
        'ui.router',
        'OfflineApp.Heartbeat',
        'OfflineApp.Remote',
        'OfflineApp.Store',
        'OfflineApp.Client'
    ]);

    app.config([
        '$stateProvider', function($stateProvider) {
            $stateProvider.state('home',
            {
                url: '/',
                templateUrl: '/app/views/home.html'
            });
        }
    ]);
})();