namespace OfflineMobileTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Clients_and_Notes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FamilyName = c.String(),
                        GivenNames = c.String(),
                        BirthDate = c.DateTime(),
                        ResidentialAddress_Address1 = c.String(),
                        ResidentialAddress_Address2 = c.String(),
                        ResidentialAddress_Locality = c.String(),
                        ResidentialAddress_State = c.String(),
                        ResidentialAddress_Postcode = c.String(),
                        PostalAddress_Address1 = c.String(),
                        PostalAddress_Address2 = c.String(),
                        PostalAddress_Locality = c.String(),
                        PostalAddress_State = c.String(),
                        PostalAddress_Postcode = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Notes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClientId = c.Guid(),
                        Created = c.DateTime(nullable: false),
                        Author = c.String(),
                        Subject = c.String(),
                        Body = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.ClientId)
                .Index(t => t.ClientId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Notes", "ClientId", "dbo.Clients");
            DropIndex("dbo.Notes", new[] { "ClientId" });
            DropTable("dbo.Notes");
            DropTable("dbo.Clients");
        }
    }
}
