﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using OfflineMobileTest.Models;

namespace OfflineMobileTest.Controllers.Api
{
    [RoutePrefix("api/Offline")]
    public class OfflineController : ApiController
    {
        protected DataContext Db { get; set; }

        public OfflineController() : base()
        {
            Db = new DataContext();
        }

        protected override void Dispose(bool disposing)
        {
            Db.Dispose();

            base.Dispose(disposing);
        }

        [HttpGet, Route("Client")]
        public IHttpActionResult List()
        {
            return Ok(Db.Clients.OrderBy(x => x.FamilyName).ThenBy(x => x.GivenNames));
        }

        [HttpGet, Route("Client/{id:guid}")]
        public IHttpActionResult GetClient(Guid id)
        {
            var obj = Db.Clients.FirstOrDefault(x => x.Id == id);

            if (obj == null)
                return NotFound();

            return Ok(obj);
        }

        [HttpPost, Route("Client/{id:guid}")]
        public async Task<IHttpActionResult> UpdateClient(Guid id, ViewModels.UpdateClient model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var obj = await Db.Clients.FirstOrDefaultAsync(x => x.Id == id);

            if (obj == null)
                return NotFound();

            if (obj.Updated >= model.Updated)
                return BadRequest("The client record on the server is newer than the one you are trying to sync");

            obj.FamilyName = model.FamilyName;
            obj.GivenNames = model.GivenNames;
            obj.BirthDate = model.BirthDate;
            obj.PostalAddress = model.PostalAddress;
            obj.ResidentialAddress = model.ResidentialAddress;
            obj.Updated = model.Updated;

            var ret = await Db.SaveChangesAsync();

            return Ok(obj);
        }
    }
}
