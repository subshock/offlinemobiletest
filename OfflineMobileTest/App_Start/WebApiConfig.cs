﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace OfflineMobileTest
{
    public partial class Startup
    {
        public static void RegisterWebApi(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
