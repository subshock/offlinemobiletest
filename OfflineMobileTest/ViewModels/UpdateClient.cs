﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OfflineMobileTest.Models.ComplexTypes;

namespace OfflineMobileTest.ViewModels
{
    public class UpdateClient
    {
        public string FamilyName { get; set; }
        public string GivenNames { get; set; }
        public DateTime? BirthDate { get; set; }
        public DateTime Updated { get; set; }
        public AddressInfo ResidentialAddress { get; set; }
        public AddressInfo PostalAddress { get; set; }
    }
}