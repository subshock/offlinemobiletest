﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(OfflineMobileTest.Startup))]

namespace OfflineMobileTest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            DatabaseConfig();
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(RegisterWebApi);
            RegisterRoutes(RouteTable.Routes);

            app.MapSignalR();

            HangfireConfig(app);

            RegisterBundles(BundleTable.Bundles);
        }
    }
}