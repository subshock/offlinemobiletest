namespace OfflineMobileTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Updated_Date : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clients", "Updated", c => c.DateTime(nullable: false));
            AddColumn("dbo.Notes", "Updated", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Notes", "Updated");
            DropColumn("dbo.Clients", "Updated");
        }
    }
}
