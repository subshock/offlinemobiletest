﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfflineMobileTest.Infrastructure
{
    interface IUpdated
    {
        DateTime Updated { get; set; }
    }
}
