﻿(function() {
    var mod = angular.module('OfflineApp.Store', ['indexedDB']);

    mod.config([
        '$indexedDBProvider', function ($indexedDBProvider) {
            $indexedDBProvider
                .connection('OfflineApp')
                .upgradeDatabase(1, function(event, db, tx) {
                    var objStore = db.createObjectStore('clients', { keyPath: 'Id' });
                });
        }
    ]);

    mod.service('StoreService', [
        '$indexedDB', function($db) {
            var service = {
                listClients: function() {
                    return $db.openStore('clients', function(store) {
                        return store.getAll();
                    });
                },
                getClient: function(id) {
                    return $db.openStore('clients', function(store) {
                        return store.find(id).then(function (obj) { return obj; }, function () { return null; });
                    });
                },
                deleteClient: function(id) {
                    return $db.openStore('clients', function(store) {
                        return store.delete(id);
                    });
                },
                updateClient: function(client) {
                    return $db.openStore('clients', function(store) {
                        return store.upsert(client);
                    });
                }
            };

            return service;
        }
    ]);
})();