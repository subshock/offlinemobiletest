﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using OfflineMobileTest.Infrastructure;
using OfflineMobileTest.Models.ComplexTypes;

namespace OfflineMobileTest.Models
{
    public class Client : IUpdated
    {
        [Key]
        public Guid Id { get; set; }

        public string FamilyName { get; set; }
        public string GivenNames { get; set; }
        public DateTime? BirthDate { get; set; }
        public AddressInfo ResidentialAddress { get; set; }
        public AddressInfo PostalAddress { get; set; }
        public DateTime Updated { get; set; }

        public Client()
        {
            ResidentialAddress = new AddressInfo();
            PostalAddress = new AddressInfo();
        }
    }
}