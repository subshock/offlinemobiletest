# OfflineMobileTest #

A proof of concept for testing how offline mobile web applications work.

This project features
* ASP.NET WebApi backend
* SignalR for monitoring connection status
* AngularJS frontend
* IndexedDB for local storage