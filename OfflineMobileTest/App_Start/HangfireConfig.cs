﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hangfire;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Infrastructure;
using OfflineMobileTest.Hub;
using Owin;

namespace OfflineMobileTest
{
    public partial class Startup
    {
        private static readonly Lazy<IHubContext> _instance = new Lazy<IHubContext>(
            () => GlobalHost.ConnectionManager.GetHubContext<HeartbeatHub>());

        public void HangfireConfig(IAppBuilder app)
        {
            GlobalConfiguration.Configuration
                .UseSqlServerStorage("DefaultConnection");

            app.UseHangfireDashboard();
            app.UseHangfireServer();

            RecurringJob.AddOrUpdate("HeartBeat", () => SendHeartbeat(), Cron.Minutely);
        }

        public static void SendHeartbeat()
        {
            _instance.Value.Clients.All.sendHeartbeat("tha-thump!");
        }
    }
}