﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using OfflineMobileTest.Models;
using OfflineMobileTest.Models.ComplexTypes;

namespace OfflineMobileTest
{
    public partial class Startup
    {
        public class JsonClientData
        {
            public string FamilyName { get; set; }
            public string GivenNames { get; set; }
            public string Address { get; set; }
            public string Locality { get; set; }
            public string State { get; set; }
            public string Postcode { get; set; }
            public DateTime? BirthDate { get; set; }
        }

        public void DatabaseConfig()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DataContext, Migrations.Configuration>());

            using (var ctx = new DataContext())
            {
                ctx.Database.Initialize(false);

                if (!ctx.Clients.Any())
                {
                    var jsonData = @"[
    {""GivenNames"":""Jaxon"",""FamilyName"":""Dooley"",""Address"":""16 Davenport Street"",""Locality"":""COOLUMBOOKA"",""State"":""NSW"",""Postcode"":""2632"",""BirthDate"":""1987-12-09""},
    {""GivenNames"":""Mikayla"",""FamilyName"":""Johnston"",""Address"":""86 Porana Place"",""Locality"":""DALWALLINU"",""State"":""WA"",""Postcode"":""6609"",""BirthDate"":""1992-11-20""},
    {""GivenNames"":""Bianca"",""FamilyName"":""Macrossan"",""Address"":""49 Learmouth Street"",""Locality"":""DRIK DRIK"",""State"":""VIC"",""Postcode"":""3304"",""BirthDate"":""1963-08-17""},
    {""GivenNames"":""Jake"",""FamilyName"":""Handcock"",""Address"":""16 Hill Street"",""Locality"":""MACQUARIE"",""State"":""TAS"",""Postcode"":""7151"",""BirthDate"":""1947-06-16""},
    {""GivenNames"":""Claudia"",""FamilyName"":""Joske"",""Address"":""61 Bayview Close"",""Locality"":""TARRAMBA"",""State"":""QLD"",""Postcode"":""4715"",""BirthDate"":""1931-04-09""},
    {""GivenNames"":""Molly"",""FamilyName"":""Reveley"",""Address"":""69 Grandis Road"",""Locality"":""KINCHELA"",""State"":""NSW"",""Postcode"":""2440"",""BirthDate"":""1953-09-07""},
    {""GivenNames"":""Emma"",""FamilyName"":""McLaughlin"",""Address"":""18 Farrar Parade"",""Locality"":""GREEN HEAD"",""State"":""WA"",""Postcode"":""6514"",""BirthDate"":""1983-06-25""},
    {""GivenNames"":""Stella"",""FamilyName"":""Ball"",""Address"":""47 Bette McNee Street"",""Locality"":""PEREKERTEN"",""State"":""NSW"",""Postcode"":""2733"",""BirthDate"":""1970-02-20""},
    {""GivenNames"":""Bethany"",""FamilyName"":""Gabriel"",""Address"":""62 Darwinia Loop"",""Locality"":""INNAWANGA"",""State"":""WA"",""Postcode"":""6751"",""BirthDate"":""1993-11-08""},
    {""GivenNames"":""Katie"",""FamilyName"":""Cassell"",""Address"":""5 Rimbanda Road"",""Locality"":""TORRINGTON"",""State"":""NSW"",""Postcode"":""2371"",""BirthDate"":""1965-03-11""},
    {""GivenNames"":""Rebecca"",""FamilyName"":""Cracknell"",""Address"":""52 Wallis Street"",""Locality"":""DACEYVILLE"",""State"":""NSW"",""Postcode"":""2032"",""BirthDate"":""1973-05-18""},
    {""GivenNames"":""Emma"",""FamilyName"":""Frost"",""Address"":""53 Raglan Street"",""Locality"":""TABLELANDS"",""State"":""QLD"",""Postcode"":""4605"",""BirthDate"":""1997-01-16""},
    {""GivenNames"":""Laura"",""FamilyName"":""Rumble"",""Address"":""40 Grayson Street"",""Locality"":""GALORE"",""State"":""NSW"",""Postcode"":""2652"",""BirthDate"":""1944-03-15""},
    {""GivenNames"":""Angelina"",""FamilyName"":""Cani"",""Address"":""16 South Street"",""Locality"":""RIDGEWAY"",""State"":""TAS"",""Postcode"":""7054"",""BirthDate"":""1940-07-12""},
    {""GivenNames"":""Sienna"",""FamilyName"":""Macadam"",""Address"":""3 Hart Street"",""Locality"":""ROUCHEL"",""State"":""NSW"",""Postcode"":""2336"",""BirthDate"":""1965-04-23""},
    {""GivenNames"":""Ryder"",""FamilyName"":""Ayers"",""Address"":""94 Eshelby Drive"",""Locality"":""MYSTERTON"",""State"":""QLD"",""Postcode"":""4812"",""BirthDate"":""1939-10-27""},
    {""GivenNames"":""Brodie"",""FamilyName"":""Bowker"",""Address"":""64 Sale-Heyfield Road"",""Locality"":""WALKERVILLE SOUTH"",""State"":""VIC"",""Postcode"":""3956"",""BirthDate"":""1983-11-04""},
    {""GivenNames"":""Zane"",""FamilyName"":""Witt"",""Address"":""37 Garden Place"",""Locality"":""GRANITE FLAT"",""State"":""VIC"",""Postcode"":""3525"",""BirthDate"":""1959-05-12""},
    {""GivenNames"":""Natasha"",""FamilyName"":""Hoffnung"",""Address"":""97 Acheron Road"",""Locality"":""SALE"",""State"":""VIC"",""Postcode"":""3850"",""BirthDate"":""1943-09-22""},
    {""GivenNames"":""Jake"",""FamilyName"":""Leslie"",""Address"":""21 Shaw Drive"",""Locality"":""EDDINGTON"",""State"":""VIC"",""Postcode"":""3472"",""BirthDate"":""1930-08-13""}
]";
                    var data = JsonConvert.DeserializeObject<IEnumerable<JsonClientData>>(jsonData);

                    foreach (var row in data)
                    {
                        ctx.Clients.Add(new Client
                        {
                            Id = Guid.NewGuid(),
                            BirthDate = row.BirthDate,
                            FamilyName = row.FamilyName,
                            GivenNames = row.GivenNames,
                            ResidentialAddress = new AddressInfo
                            {
                                Address1 = row.Address,
                                Locality = row.Locality,
                                State = row.State,
                                Postcode = row.Postcode
                            },
                            PostalAddress = new AddressInfo()
                            {
                                Address1 = row.Address,
                                Locality = row.Locality,
                                State = row.State,
                                Postcode = row.Postcode
                            }
                        });
                    }

                    ctx.SaveChanges();
                }
            }
        }
    }
}