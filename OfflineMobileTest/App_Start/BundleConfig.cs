﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace OfflineMobileTest
{
    public partial class Startup
    {
        public void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = true;

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/bootstrap-theme.css",
                "~/Content/fa/css/font-awesome.css",
                "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/Scripts/core").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/underscore.min.js",
                "~/Scripts/bootstrap.js",
                "~/Scripts/angular.*",
                "~/Scripts/angular-animate.*",
                "~/Scripts/angular-ui-router.*",
                "~/Scripts/angular-indexed-db.js",
                "~/Scripts/jquery.signalR-{version}.js"));

            bundles.Add(new ScriptBundle("~/app/application")
                .IncludeDirectory("~/app", "*.js", true));
        }
    }
}