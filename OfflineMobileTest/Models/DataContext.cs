﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Linq;
using System.Web;
using OfflineMobileTest.Infrastructure;

namespace OfflineMobileTest.Models
{
    public class DataContext : DbContext
    {
        public DataContext() : base("DefaultConnection")
        {
            ((IObjectContextAdapter) this).ObjectContext.SavingChanges += OnSavingChanges;
        }

        private void OnSavingChanges(object sender, EventArgs eventArgs)
        {
            ChangeTracker.DetectChanges();

            var currentDate = DateTime.UtcNow;

            foreach (var entry in ChangeTracker.Entries<IUpdated>())
            {
                entry.Entity.Updated = currentDate;
            }
        }

        public IDbSet<Client> Clients { get; set; }
        public IDbSet<Note> Notes { get; set; } 
    }
}