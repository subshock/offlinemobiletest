﻿(function() {
    var mod = angular.module('OfflineApp.Heartbeat', []);

    mod.service('HeartbeatService', [
        '$log', '$rootScope', '$timeout', function($log, $rootScope, $timeout) {
            var states = { Connecting: 0, Online: 1, Reconnecting: 2, Offline: 4 }
            var connecting = false;
            var hub = $.connection.heartbeatHub;

            hub.client.sendHeartbeat = function(msg) {
                $log.info('Heartbeat: ' + msg);
            }

            $.connection.hub.stateChanged(function(state) {
                $timeout(function() { $rootScope.$broadcast('$onlineStateChanged', state); }, 0);
            });

            function startHub() {
                if ($.connection.hub.state !== states.Offline) {
                    return;
                }

                connecting = true;

                $.connection.hub.start().done(function() {
                    $log.info('Connected, transport: ' + $.connection.hub.transport.name);
                    connecting = false;
                }).fail(function () {
                    connecting = false;
                });
            }

            startHub();

            var service = {
                state: states,
                status: function () { return $.connection.hub.state; },
                reconnect: startHub,
                connecting: function() { return connecting; }
            };

            return service;
        }
    ]);

    mod.directive('heartbeatMonitor', function() {
        return {
            restrict: 'AE',
            templateUrl: '/app/modules/heartbeat/heartbeat.directive.html',
            controller: 'heartbeatMonitorCtl'
        }
    });

    mod.controller('heartbeatMonitorCtl', [
        '$scope', 'HeartbeatService', '$log', function($scope, HeartbeatService, $log) {

            $scope.reconnect = function() {
                HeartbeatService.reconnect();
            };

            $scope.$on('$onlineStateChanged', function (event, state) {
                $log.info('directive: oldState: ' + state.oldState + ', newState: ' + state.newState);
                updateStatus(state.newState);
            });

            function updateStatus(newState) {
                $scope.status = newState;
                switch (newState) {
                    case HeartbeatService.state.Connecting:
                        $scope.message = 'Connecting'; break;
                    case HeartbeatService.state.Online:
                        $scope.message = 'Online'; break;
                    case HeartbeatService.state.Reconnecting:
                        $scope.message = 'Reconnecting'; break;
                    case HeartbeatService.state.Offline:
                        $scope.message = 'Offline'; break;
                }

                $log.info('status:' + $scope.status + ' message: ' + $scope.message);
            }

            updateStatus(HeartbeatService.status());
        }
    ]);
})();