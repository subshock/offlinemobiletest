﻿(function () {
    var mod = angular.module('OfflineApp.Remote', ['ui.router']);

    mod.config([
        '$stateProvider', function ($stateProvider) {
            $stateProvider
                .state('remote', {
                    url: '/remote',
                    template: '<ui-view></ui-view>',
                    controller: ['$state', function ($state) {
                        if ($state.current.name === 'remote') {
                            $state.go('remote.list');
                        }
                    }]
                })
                .state('remote.list', {
                    url: '/list',
                    templateUrl: '/app/views/remote.html',
                    controller: 'RemoteStateController'
                })
                .state('remote.client', {
                    url: '/client/:id',
                    templateUrl: '/app/views/remote-client.html',
                    controller: 'RemoteClientStateController'
                });
        }
    ]);

    mod.service('RemoteService', [
        '$http', function ($http) {
            var apiBase = '/api/Offline/';

            var service = {
                listClients: function () { return $http.get(apiBase + 'Client').then(function (response) { return response.data; }); },
                getClient: function (id) { return $http.get(apiBase + 'Client/' + id).then(function (response) { return response.data; }) },
                updateClient: function (client) { return $http.post(apiBase + 'Client/' + client.Id, client).then(function (response) { return response.data; }); }
            };

            return service;
        }
    ]);

    mod.controller('RemoteStateController', [
        '$q', '$scope', '$timeout', 'HeartbeatService', 'StoreService', 'RemoteService', function ($q, $scope, $timeout, HeartbeatService, StoreService, RemoteService) {
            $scope.clients = [];
            $scope.localClients = [];
            $scope.loading = false;

            $scope.loadClients = function () {
                $scope.loading = true;

                // if we are in the process of connecting then wait and try again
                if (HeartbeatService.connecting()) {
                    $timeout($scope.loadClients, 1000);
                } else {
                    loadClientsInternal();
                }
            };

            $scope.addLocalClientState = function () {
                for (var i = 0; i !== $scope.clients.length; i++) {
                    var client = $scope.clients[i];
                    var lclient = _.findWhere($scope.localClients, { Id: client.Id });

                    if (lclient) {
                        if (client.Updated > lclient.Updated) {
                            client.LocalState = 'stale';
                        } else if (client.Updated < lclient.Updated) {
                            client.LocalState = 'pending';
                        } else if (client.Updated == lclient.Updated) {
                            client.LocalState = 'loaded';
                        }
                    } else {
                        client.LocalState = 'none';
                    }
                }
            }

            function loadClientsInternal() {
                if (HeartbeatService.status() !== HeartbeatService.state.Online) {
                    $scope.error = 'You must be online to be able to view remote clients';
                    $scope.loading = false;
                    return;
                }

                $scope.error = null;

                $q.all([
                    RemoteService.listClients().then(function (data) {
                        $scope.clients = data;
                        $scope.loading = false;
                    }, function (response) {
                        $scope.error = response.error;
                    }), StoreService.listClients().then(function (data) {
                        $scope.localClients = data;
                    })
                ]).then(function () {
                    $scope.addLocalClientState();
                    $scope.loading = false;
                });
            }

            function init() {
                $scope.loadClients();
            }

            init();
        }
    ]);

    mod.controller('RemoteClientStateController', [
        '$q', '$scope', '$timeout', '$state', '$stateParams', 'HeartbeatService', 'StoreService', 'RemoteService', function ($q, $scope, $timeout, $state, $stateParams, HeartbeatService, StoreService, RemoteService) {
            $scope.client = {};
            $scope.lclient = null;
            $scope.loading = true;

            $scope.loadOffline = function () {
                StoreService.updateClient($scope.client)
                    .then(function() {
                        loadClient();
                    });
            };

            $scope.unloadOffline = function() {
                StoreService.deleteClient($scope.client.Id)
                    .then(function() {
                        loadClient();
                    });
            };

            $scope.syncRemote = function() {
                RemoteService.updateClient($scope.lclient)
                    .then(function (data) {
                        loadClient();
                    });
            };

            function loadClient() {
                if (HeartbeatService.status() === HeartbeatService.state.Connecting) {
                    $timeout(loadClient, 1000);
                } else {
                    $scope.loading = true;

                    $q.all([
                        RemoteService.getClient($stateParams.id).then(function (data) {
                            $scope.client = data;
                        }),
                        StoreService.getClient($stateParams.id).then(function (data) {
                            $scope.lclient = data;
                        })
                    ]).then(function () {
                        if ($scope.lclient) {
                            $scope.loaded = true;

                            if ($scope.lclient.Updated > $scope.client.Updated) {
                                $scope.status = 'Pending';
                            } else if ($scope.lclient.Updated < $scope.client.Updated) {
                                $scope.status = 'Stale';
                            } else if ($scope.lclient.Updated == $scope.client.Updated) {
                                $scope.status = 'Loaded';
                            }
                        } else {
                            $scope.loaded = false;
                        }
                        $scope.loading = false;
                    });
                }
            }

            function init() {
                if (!$stateParams.id) {
                    $state.go('remote');
                }

                loadClient();
            }

            init();
        }
    ]);
})();