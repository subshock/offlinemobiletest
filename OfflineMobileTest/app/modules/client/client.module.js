﻿(function() {
    var mod = angular.module('OfflineApp.Client', []);

    mod.config([
        '$stateProvider', function($stateProvider) {
            $stateProvider.state('clients', {
                    url: '/clients',
                    template: '<ui-view></ui-view>',
                    controller: ['$state', function($state) {
                        if ($state.current.name === 'clients') {
                            $state.go('clients.list');
                        }
                    }]
                })
                .state('clients.list', {
                    url: '/list',
                    templateUrl: '/app/views/clients-list.html',
                    controller: 'ClientListController'
                })
                .state('clients.update', {
                    url: '/:id',
                    templateUrl: '/app/views/clients-update.html',
                    controller: 'ClientUpdateController'
                });
        }
    ]);

    mod.controller('ClientListController', [
        '$scope', 'StoreService',
        function($scope, StoreService) {
            $scope.clients = [];

            function init() {
                $scope.loading = true;

                StoreService.listClients().then(function (data) {
                    $scope.clients = data;
                    $scope.loading = false;
                }, function() {
                    $scope.clients = [];
                    $scope.loading = false;
                });
            }

            init();
        }
    ]);

    mod.controller('ClientUpdateController', [
        '$scope', '$state', '$stateParams', 'StoreService', function($scope, $state, $stateParams, StoreService) {
            $scope.client = {};
            $scope.loading = false;
            $scope.stateList = [
                { id: 'ACT', name: 'Australian Capital Territory' },
                { id: 'NSW', name: 'New South Wales' },
                { id: 'QLD', name: 'Queensland' },
                { id: 'SA', name: 'South Australia' },
                { id: 'TAS', name: 'Tasmania' },
                { id: 'VIC', name: 'Victoria' },
                { id: 'WA', name: 'Western Australia' },
                { id: 'Other', name: 'Other' }
            ];

            $scope.save = function () {
                $scope.client._Changed = true;
                $scope.client.Updated = new Date().toISOString().replace('Z', '');
                StoreService.updateClient($scope.client).then(function() {
                    $state.go('clients.list');
                });
            };

            function init() {
                $scope.loading = true;

                if (!$stateParams.id) {
                    $state.go('clients.list');
                    return;
                }

                StoreService.getClient($stateParams.id)
                    .then(function(data) {
                        $scope.client = data;
                        $scope.loading = false;
                    }, function() {
                        $state.go('clients.list');
                    });
            }

            init();
        }
    ]);
})();