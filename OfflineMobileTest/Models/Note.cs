﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using OfflineMobileTest.Infrastructure;

namespace OfflineMobileTest.Models
{
    public class Note : IUpdated
    {
        [Key]
        public Guid Id { get; set; }

        [ForeignKey("ClientId")]
        public Client Client { get; set; }
        public Guid? ClientId { get; set; }

        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public string Author { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}